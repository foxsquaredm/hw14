// Copyright Epic Games, Inc. All Rights Reserved.

#include "HW14GameMode.h"
#include "HW14Character.h"
#include "UObject/ConstructorHelpers.h"

AHW14GameMode::AHW14GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
