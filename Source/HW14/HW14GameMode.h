// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HW14GameMode.generated.h"

UCLASS(minimalapi)
class AHW14GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHW14GameMode();
};



