// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "AttributeSet.h"
#include "HW14AttributeSet.generated.h"

	#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

	
UCLASS()
class HW14_API UHW14AttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:
	UHW14AttributeSet();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(BlueprintReadOnly, Category="Health", ReplicatedUsing= OnRep_Health)
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UHW14AttributeSet,Health)

	UPROPERTY(BlueprintReadOnly, Category="Health", ReplicatedUsing= OnRep_MaxHealth)
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UHW14AttributeSet,MaxHealth)

	UPROPERTY(BlueprintReadOnly, Category="Energy", ReplicatedUsing= OnRep_Energy)
	FGameplayAttributeData Energy;
	ATTRIBUTE_ACCESSORS(UHW14AttributeSet,Energy)

	UPROPERTY(BlueprintReadOnly, Category="Energy", ReplicatedUsing= OnRep_MaxEnergy)
	FGameplayAttributeData MaxEnergy;
	ATTRIBUTE_ACCESSORS(UHW14AttributeSet,MaxEnergy)

	UPROPERTY(BlueprintReadOnly, Category="Power", ReplicatedUsing= OnRep_Power)
	FGameplayAttributeData Power;
	ATTRIBUTE_ACCESSORS(UHW14AttributeSet,Power)

	UPROPERTY(BlueprintReadOnly, Category="TotalLevel", ReplicatedUsing= OnRep_TotalLevel)
	FGameplayAttributeData TotalLevel;
	ATTRIBUTE_ACCESSORS(UHW14AttributeSet,TotalLevel)
	
	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
		
protected:

	void AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxAttributeValue, const FGameplayAttribute& EffectedAttributeProperty);
	
	UFUNCTION()
	virtual void OnRep_Health(const FGameplayAttributeData& OldHealth);

	UFUNCTION()
	virtual void OnRep_MaxHealth(const FGameplayAttributeData& OldMaxHealth);

	UFUNCTION()
	virtual void OnRep_Energy(const FGameplayAttributeData& OldEnergy);

	UFUNCTION()
	virtual void OnRep_MaxEnergy(const FGameplayAttributeData& OldMaxEnergy);

	UFUNCTION()
	virtual void OnRep_Power(const FGameplayAttributeData& OldPower);

	UFUNCTION()
	virtual void OnRep_TotalLevel(const FGameplayAttributeData& OldTotalLevel);
};
