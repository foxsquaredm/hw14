// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EHW14AbilityInputID : uint8
{
	None,
	Confirm,
	Cancel,
	Attack,
	Ultimate,
	LevelUp
};
