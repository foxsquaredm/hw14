// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HW14.h"
#include "Abilities/GameplayAbility.h"
#include "HW14GameplayAbility.generated.h"

UCLASS()
class HW14_API UHW14GameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()


public:
	UHW14GameplayAbility();

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category="Ability")
	EHW14AbilityInputID AbilityInputID = EHW14AbilityInputID::None;
};
