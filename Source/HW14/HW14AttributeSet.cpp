// Fill out your copyright notice in the Description page of Project Settings.


#include "HW14AttributeSet.h"
#include "Net/UnrealNetwork.h"

UHW14AttributeSet::UHW14AttributeSet()
{
	
}

void UHW14AttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME_CONDITION_NOTIFY(UHW14AttributeSet, Health, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UHW14AttributeSet, MaxHealth, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UHW14AttributeSet, Energy, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UHW14AttributeSet, MaxEnergy, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UHW14AttributeSet, Power, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UHW14AttributeSet, TotalLevel, COND_None, REPNOTIFY_Always);
}

void UHW14AttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);

	if (Attribute == GetMaxHealthAttribute())
	{
		// UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();
		//
		// const float CurrentMaxHealthValue = MaxHealth.GetCurrentValue();
		// if (!FMath::IsNearlyEqual(CurrentMaxHealthValue,NewValue) && AbilityComp)
		// {
		// 	const float CurrentHealthValue = Health.GetCurrentValue();
		// 	float NewHealthValue = CurrentHealthValue * NewValue / CurrentMaxHealthValue;
		// 	AbilityComp->ApplyModToAttributeUnsafe(GetHealthAttribute(), EGameplayModOp::Override, NewHealthValue);
		// }
		AdjustAttributeForMaxChange(Health, MaxHealth, NewValue, GetHealthAttribute());
	}
	else if (Attribute == GetMaxEnergyAttribute())
	{
		// UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();
		//
		// const float CurrentMaxEnergyValue = MaxEnergy.GetCurrentValue();
		// if (!FMath::IsNearlyEqual(CurrentMaxEnergyValue,NewValue) && AbilityComp)
		// {
		// 	const float CurrentEnergyValue = Energy.GetCurrentValue();
		// 	float NewEnergyValue = CurrentEnergyValue * NewValue / CurrentMaxEnergyValue;
		// 	AbilityComp->ApplyModToAttributeUnsafe(GetEnergyAttribute(), EGameplayModOp::Override, NewEnergyValue);
		// }
		AdjustAttributeForMaxChange(Energy, MaxEnergy, NewValue, GetEnergyAttribute());
	}
}

void UHW14AttributeSet::AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute,
	const FGameplayAttributeData& MaxAttribute, float NewMaxAttributeValue,
	const FGameplayAttribute& AffectedAttributeProperty)
{
	UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();
	const float CurrentMaxAttributeValue = MaxAttribute.GetCurrentValue() == 0 ?NewMaxAttributeValue : MaxAttribute.GetCurrentValue();
	if (!FMath::IsNearlyEqual(CurrentMaxAttributeValue,NewMaxAttributeValue) && AbilityComp)
	{
		const float CurrentAffectedAttributeValue = AffectedAttribute.GetCurrentValue();
		float NewCurrentValue = CurrentAffectedAttributeValue * NewMaxAttributeValue / CurrentMaxAttributeValue;
		AbilityComp->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Override, NewCurrentValue);
	}
}

void UHW14AttributeSet::OnRep_Health(const FGameplayAttributeData& OldHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UHW14AttributeSet, Health, OldHealth);
}

void UHW14AttributeSet::OnRep_MaxHealth(const FGameplayAttributeData& OldMaxHealth)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UHW14AttributeSet, MaxHealth, OldMaxHealth);
}

void UHW14AttributeSet::OnRep_Energy(const FGameplayAttributeData& OldEnergy)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UHW14AttributeSet, Energy, OldEnergy);
}

void UHW14AttributeSet::OnRep_MaxEnergy(const FGameplayAttributeData& OldMaxEnergy)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UHW14AttributeSet, Energy, OldMaxEnergy);
}

void UHW14AttributeSet::OnRep_Power(const FGameplayAttributeData& OldPower)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UHW14AttributeSet, Power, OldPower);
}

void UHW14AttributeSet::OnRep_TotalLevel(const FGameplayAttributeData& OldTotalLevel)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UHW14AttributeSet, TotalLevel, OldTotalLevel);
}
